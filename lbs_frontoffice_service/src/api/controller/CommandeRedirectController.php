<?php
/**
 * CommandeRedirectController.php
 * 
 * Controller qui gère les méthodes de redirection du front office vers le service de commande
 */

namespace lbs\frontoffice\api\controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \lbs\frontoffice\api\utils\JsonWriter;
use Slim\Container;
use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\RequestOptions;

/**
 * Class CommandeRedirectController
 * @package lbs\frontoffice\api\controller
 */
class CommandeRedirectController {

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Redirige la requête de consultation de commande au service lbs_commande
     */
    public function redirectToCommand(Request $req, Response $resp, $args) {
        
        // Envoi de la requête de récupération d'une commande
        $client = new Client([
            'base_uri' => $this->container->get('settings')['command_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->get('/commands/' . $args["id"], [
                //Vérification sur l'existence du header X-lbs-token
                'headers' => ['X-lbs-token' => !empty($req->getHeader('X-lbs-token')) ? $req->getHeader('X-lbs-token') : null],
                'query' => $req->getQueryParams()
            ]);
        }
        catch(ServerException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return $resp->withStatus($response->getStatusCode())
                    ->withHeader('Content-Type', $response->getHeader('Content-Type'))
                    ->withBody($response->getBody());
    }

    /**
     * Redirige la requête de consultation d'items
     */
    public function redirectToCommandItems(Request $req, Response $resp, $args) {
        
        // Envoi de la requête de récupération des items d'une commande
        $client = new Client([
            'base_uri' => $this->container->get('settings')['command_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->get('/commands/' . $args["id"] . '/items', [
                // Vérification sur l'existence du header X-lbs-token
                'headers' => ['X-lbs-token' => !empty($req->getHeader('X-lbs-token')) ? $req->getHeader('X-lbs-token') : null],
                'query' => $req->getQueryParams()
            ]);
        }
        catch(ServerException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return $resp->withStatus($response->getStatusCode())
                    ->withHeader('Content-Type', $response->getHeader('Content-Type'))
                    ->withBody($response->getBody());
    }

    /**
     * Redirige la requête de creation de commande
     */
    public function redirectToCreateCommand(Request $req, Response $resp, $args) {
        
        $client = new Client([
            'base_uri' => $this->container->get('settings')['command_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->post('/commands/', [
                RequestOptions::JSON => $req->getParsedBody()
            ]);
        }
        catch(ServerException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            $code = (isset($data["error"])) ? $data["error"] : $data["code"];

            return JsonWriter::jsonOutput($resp, $code, $data);
        }
        catch(ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            $code = (isset($data["error"])) ? $data["error"] : $data["code"];

            return JsonWriter::jsonOutput($resp, $code, $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => $e->getMessage()
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return $resp->withStatus($response->getStatusCode())
                    ->withHeader('Content-Type', $response->getHeader('Content-Type'))
                    ->withBody($response->getBody());
    }

    /**
     * Redirige la requête de paiement de commande
     */
    public function redirectToOrderCommand(Request $req, Response $resp, $args) {
        
        $client = new Client([
            'base_uri' => $this->container->get('settings')['command_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->put('/commands/' . $args["id"], [
                // Vérification sur l'existence du header X-lbs-token
                'headers' => ['X-lbs-token' => !empty($req->getHeader('X-lbs-token')) ? $req->getHeader('X-lbs-token') : null],
                'query' => $req->getQueryParams(),
                RequestOptions::JSON => $req->getParsedBody()
            ]);
        }
        catch(ServerException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            $code = (isset($data["error"])) ? $data["error"] : $data["code"];

            return JsonWriter::jsonOutput($resp, $code, $data);
        }
        catch(ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            $code = (isset($data["error"])) ? $data["error"] : $data["code"];

            return JsonWriter::jsonOutput($resp, $code, $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => $e->getMessage()
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return $resp->withStatus($response->getStatusCode())
                    ->withHeader('Content-Type', $response->getHeader('Content-Type'))
                    ->withBody($response->getBody());
    }
}

?>