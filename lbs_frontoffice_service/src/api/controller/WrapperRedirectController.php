<?php
/**
 * CommandeRedirectController.php
 * 
 * Controller qui gère les méthodes de redirection du front office vers le service de wrapper (catalogue)
 */

namespace lbs\frontoffice\api\controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \lbs\frontoffice\api\utils\JsonWriter;
use Slim\Container;
use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\RequestOptions;

/**
 * Class WrapperRedirectController
 * @package lbs\frontoffice\api\controller
 */
class WrapperRedirectController {

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Redirige la requête de consultation de sandwich
     */
    public function redirectToSandwich(Request $req, Response $resp, $args) {
        
        $client = new Client([
            'base_uri' => $this->container->get('settings')['wrapper_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->get('/sandwiches/' . $args["id"]);
        }
        catch(ServerException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return $resp->withStatus($response->getStatusCode())
                    ->withHeader('Content-Type', $response->getHeader('Content-Type'))
                    ->withBody($response->getBody());
    }

    /**
     * Redirige la requête de consultation de catégories
     */
    public function redirectToCategories(Request $req, Response $resp, $args) {
        
        $client = new Client([
            'base_uri' => $this->container->get('settings')['wrapper_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->get('/categories/');
        }
        catch(ServerException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return $resp->withStatus($response->getStatusCode())
                    ->withHeader('Content-Type', $response->getHeader('Content-Type'))
                    ->withBody($response->getBody());
    }

    /**
     * Redirige la requête de consultation de sandwich d'une catégorie
     */
    public function redirectToSandwiches(Request $req, Response $resp, $args) {
        
        $client = new Client([
            'base_uri' => $this->container->get('settings')['wrapper_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->get('/categories/' . $args["libelle"]);
        }
        catch(ServerException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return $resp->withStatus($response->getStatusCode())
                    ->withHeader('Content-Type', $response->getHeader('Content-Type'))
                    ->withBody($response->getBody());
    }
}

?>