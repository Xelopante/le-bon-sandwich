<?php

use lbs\frontoffice\api\utils\JsonWriter;

return [
    "notFoundHandler" => function($container) {
        return function($req, $resp) {
            $uri = $req->getUri();
            $data = [
                "type" => "error",
                "error" => 400,
                "message" => "The request ".$uri->getScheme()."://".$uri->getHost().$uri->getPath()." is not recognized : malformed URI"
            ];
            
            $resp = JsonWriter::jsonOutput($resp, 400, $data);

            return $resp;
        };
    },
    "notAllowedHandler" => function($container) {
        return function($req, $resp, $methods) {

            $data = [
                "type" => "error",
                "error" => 405,
                "message" => $req->getOriginalMethod()." method not allowed. Allowed methods are : ".implode(',', $methods)
            ];

            $resp = JsonWriter::jsonOutput($resp, 405, $data);

            return $resp;
        };
    },
    "phpErrorHandler" => function($container) {
        return function($req, $resp, $error) {

            $data = [
                "error" => $error->getMessage(),
                "file" => $error->getFile(),
                "line" => $error->getLine()
            ];

            $resp = JsonWriter::jsonOutput($resp, 500, $data);

            return $resp ;
        };
    }
];

?>