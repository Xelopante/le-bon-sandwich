<?php
/**
 * File:  index.php
 *
 */

require_once  __DIR__ . '/../src/vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Configuration de Slim (réécriture des erreurs + settings du container Slim)
$errors = require_once __DIR__ . '/../src/api/errors/errors.php';
$settings = require_once __DIR__ . '/../src/api/settings/settings.php';

$slim_config = array_merge($errors, $settings);

$slim_container = new \Slim\Container($slim_config);

// Initialisation de l'application Slim
$app = new \Slim\App($slim_container);

// Ajout du contrôle CORS
$app->options('/{routes:.+}', function( Request $req, Response $resp, array $args) {
    return $resp;
});
 
$app->add(\lbs\frontoffice\api\middlewares\Cors::class . ":corsHeaders");
 

// Commande Service
$app->get('/commands/{id}[/]', \lbs\frontoffice\api\controller\CommandeRedirectController::class . ':redirectToCommand')->setName('getCommand');

$app->get('/commands/{id}/items[/]', \lbs\frontoffice\api\controller\CommandeRedirectController::class . ':redirectToCommandItems')->setName('getCommandItems');

$app->post('/commands[/]', \lbs\frontoffice\api\controller\CommandeRedirectController::class . ':redirectToCreateCommand')->setName('newCommand');

$app->put('/commands/{id}[/]', \lbs\frontoffice\api\controller\CommandeRedirectController::class . ':redirectToOrderCommand')->setName('orderCommand');


// Wrapper Service
$app->get('/sandwiches/{id}[/]', lbs\frontoffice\api\controller\WrapperRedirectController::class . ":redirectToSandwich")->setName('getSandwich');

$app->get('/categories[/]', lbs\frontoffice\api\controller\WrapperRedirectController::class . ":redirectToCategories")->setName('getCategories');

$app->get('/categories/{libelle}[/]', lbs\frontoffice\api\controller\WrapperRedirectController::class . ":redirectToSandwiches")->setName('getCategorieSandwiches');

$app->run();