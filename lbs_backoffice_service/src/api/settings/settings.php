<?php

return [
    "settings" => [
        "displayErrorDetails" => true,
        "auth_service" => "http://api.auth.local:80",
        "fabrication_service" => "http://api.fabrication.local:80"
    ]
];

?>