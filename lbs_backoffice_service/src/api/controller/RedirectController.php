<?php

namespace lbs\backoffice\api\controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \lbs\backoffice\api\utils\JsonWriter;
use Slim\Container;
use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

/**
 * Class RedirectController
 * @package lbs\backoffice\api\controller
 */
class RedirectController {

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Redirige la requête d'authentifiaction vers lbs_authentification_service
     */
    public function redirectToAuth(Request $req, Response $resp, $args) : Response {
        
        // Requête au service d'authentification pour créer l'access-token
        $client = new Client([
            'base_uri' => $this->container->get('settings')['auth_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->post('/auth', [
                'headers' => ['Authorization' => $req->getHeader('Authorization')]
            ]);
        }
        catch(ServerException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return $resp->withStatus($response->getStatusCode())
                    ->withHeader('Content-Type', $response->getHeader('Content-Type'))
                    ->withBody($response->getBody());
    }

    /**
     * Redirige la requête de récupération de commande vers lbs_commande_service en passant par lbs_authentification_service pour vérifier le token d'authentification
     */
    public function redirectToCommands(Request $req, Response $resp, $args) : Response {
        
        // Requête au service d'authentification pour vérifier l'access-token
        $client = new Client([
            'base_uri' => $this->container->get('settings')['auth_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->get('/check', [
                'headers' => ['Authorization' => $req->getHeader('Authorization')]
            ]);
        }
        catch(ServerException | ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        // On vérifie que la réponse indique que le token est valide
        $response_data = json_decode($response->getBody(), true);

        if(empty($response_data["user"]) || $response->getStatusCode() != 200) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        // Requête au service de fabrication pour récupérer les commandes
        $client = new Client([
            'base_uri' => $this->container->get('settings')['fabrication_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->get('/commands', [
                'query' => $req->getQueryParams(),
                'headers' => ['Authorization' => $req->getHeader('Authorization')]
            ]);
        }
        catch(ServerException | ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return $resp->withStatus($response->getStatusCode())
                    ->withHeader('Content-Type', $response->getHeader('Content-Type'))
                    ->withBody($response->getBody());
    }
}

?>