<?php

namespace lbs\backoffice\api\middlewares;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \lbs\backoffice\api\utils\JsonWriter;

/**
 * Class Cors
 * @package lbs\backoffice\api\middlewares
 */
class Cors {

    /**
     * Vérifie la présence du header Origin et retourne les informations relatives à la gestion des requête CORS
     */
    public function corsHeaders(Request $req, Response $resp, callable $next ): Response {

        // Gestion du header Origin stricte
        if (!$req->hasHeader('Origin')) {
            $data = [
                "type" => "error",
                "error" => 401,
                "message" => "Missing Origin Header (CORS)"
            ];

            return JsonWriter::jsonOutput($resp, 401, $data);
        }

        $response = $next($req, $resp);
        
        $response = $response
            ->withHeader('Access-Control-Allow-Origin', $req->getHeader('Origin'))
            ->withHeader('Access-Control-Allow-Methods', 'POST, PUT, GET')
            ->withHeader('Access-Control-Allow-Headers', 'Authorization')
            ->withHeader('Access-Control-Max-Age', 3600)
            ->withHeader('Access-Control-Allow-Credentials', 'true');

            return $response;
    }
}

?>