<?php
/**
 * File:  index.php
 *
 */

require_once  __DIR__ . '/../src/vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Configuration de Slim (réécriture des erreurs + settings du container Slim)
$errors = require_once __DIR__ . '/../src/api/errors/errors.php';
$settings = require_once __DIR__ . '/../src/api/settings/settings.php';

$slim_config = array_merge($errors, $settings);

$slim_container = new \Slim\Container($slim_config);

// Initialisation de l'application Slim
$app = new \Slim\App($slim_container);

// Ajout du contrôle CORS
$app->options('/{routes:.+}', function( Request $req, Response $resp, array $args) {
    return $resp;
});

$app->add(\lbs\backoffice\api\middlewares\Cors::class . ":corsHeaders");

// Redirection du service d'authentification
$app->post('/auth[/]', \lbs\backoffice\api\controller\RedirectController::class . ":redirectToAuth")->setName("authentification");

// Redirection du service de fabrication
$app->get('/commands[/]', \lbs\backoffice\api\controller\RedirectController::class . ":redirectToCommands")->setName("getCommands");

$app->run();