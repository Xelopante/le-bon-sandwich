<?php

namespace lbs\auth\api\models;
use \Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package lbs\auth\api\models
 */
class User extends Model {
    
    protected $table = 'user';
    protected $primaryKey = 'id';

    /**
     *  Récupère les données de l'utilisateur par son mail
     */
    public static function getUserByMail(string $mail) {

        return self::select('id', 'email', 'username', 'passwd', 'refresh_token', 'level')
        ->where('email', '=', $mail)
        ->firstOrFail();
    }

    /**
     *  Récupère les données de l'utilisateur par son mail
     */
    public static function getUserBaseByMail(string $mail) {

        return self::select('email', 'username', 'level')
        ->where('email', '=', $mail)
        ->firstOrFail();
    }
}

?>