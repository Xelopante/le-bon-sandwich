<?php
/**
 * Created by PhpStorm.
 * User: canals5
 * Date: 18/11/2019
 * Time: 15:27
 */

namespace lbs\auth\api\controller;

use Firebase\JWT\BeforeValidException;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\Key;
use Firebase\JWT\SignatureInvalidException;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use lbs\auth\api\models\User;
use lbs\auth\api\utils\JsonWriter;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Container;

/**
 * Class LBSAuthController
 * @package lbs\command\api\controller
 */
class LBSAuthController {

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Récupère les informations d'authentification d'un utilisateur via le header Authorization (Basic Auth) et retourne le token d'accès à l'API de fabrication
     */
    public function authenticate(Request $req, Response $resp, $args): Response {

        // Vérifie que le header Authorization est présent
        if (!$req->hasHeader('Authorization')) {

            $resp = $resp->withHeader('WWW-authenticate', 'Basic realm="commande_api api" ');

            $data = [
                "type" => "error",
                "error" => 401,
                "message" => "No Authorization header present"
            ];

            return JsonWriter::jsonOutput($resp, 401, $data);
        };

        // Déocdage du header pour récupérer les infos de connexion de l'utilisateur
        $authstring = base64_decode(explode(" ", $req->getHeader('Authorization')[0])[1]);
        list($email, $pass) = explode(':', $authstring);

        // Récupère les infos de l'utilisateur avec les credential du header Authorization
        try {
            $user = User::getUserByMail($email);

            if (!password_verify($pass, $user->passwd))
                throw new \Exception("password check failed");

            unset ($user->passwd);
        }
        catch (ModelNotFoundException $e) {
            $resp = $resp->withHeader('WWW-authenticate', 'Basic realm="lbs auth" ');

            $data = [
                "type" => "error",
                "error" => 401,
                "message" => "User not found"
            ];

            return JsonWriter::jsonOutput($resp, 401, $data);
        }
        catch (\Exception $e) {
            $resp = $resp->withHeader('WWW-authenticate', 'Basic realm="lbs auth" ');

            $data = [
                "type" => "error",
                "error" => 401,
                "message" => "Authentification error"
            ];

            return JsonWriter::jsonOutput($resp, 401, $data);
        }

        // Création d'access-token et refresh-token
        $secret = $this->container->settings['secret'];
        $token = JWT::encode(['iss' => 'http://api.auth.local/auth',
            'aud' => 'http://api.backoffice.local',
            'iat' => time(),
            'exp' => time() + (12 * 30 * 24 * 3600),
            'upr' => [
                'email' => $user->email,
                'username' => $user->username,
                'level' => $user->level
            ]],
            $secret, 'HS512');

        $user->refresh_token = bin2hex(random_bytes(32));
        $user->save();
        $data = [
            'access-token' => $token,
            'refresh-token' => $user->refresh_token
        ];

        return JsonWriter::jsonOutput($resp, 200, $data);
    }

    /**
     * Vérifie la validité de l'access-token fourni dans le header Authorization (Bearer token) et retourne les infos de l'utilisateur si valide
     */
    public function checkJWTToken(Request $req, Response $resp, $args) : Response {

        // Vérifie que le header Authorization est présent
        if (!$req->hasHeader('Authorization')) {

            $resp = $resp->withHeader('WWW-authenticate', 'Basic realm="commande_api api" ');

            $data = [
                "type" => "error",
                "error" => 401,
                "message" => "No Authorization header present"
            ];

            return JsonWriter::jsonOutput($resp, 401, $data);
        };

        // Décodage du token pour récupérer les infos de l'utilisateur
        try {
            $secret = $this->container->settings['secret'];
            $h = $req->getHeader('Authorization')[0];
            $tokenstring = sscanf($h, "Bearer %s")[0];

            $token = JWT::decode($tokenstring, new Key($secret,'HS512'));

            $mail = $token->upr->email;
            $user = User::getUserBaseByMail($mail);
        }
        catch (ExpiredException | SignatureInvalidException | ModelNotFoundException | BeforeValidException | \UnexpectedValueException $e) {
            $resp = $resp->withHeader('WWW-authenticate', 'Basic realm="lbs auth" ');

            $data = [
                "type" => "error",
                "error" => 401,
                "message" => $e->getMessage()
            ];

            return JsonWriter::jsonOutput($resp, 401, $data);
        }
        catch (\Exception $e) {
            $resp = $resp->withHeader('WWW-authenticate', 'Basic realm="lbs auth" ');

            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        $data = [
            "user" => $user
        ];

        return JsonWriter::jsonOutput($resp, 200, $data);
    }
}