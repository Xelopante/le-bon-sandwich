<?php
/**
 * File:  index.php
 *
 */

require_once  __DIR__ . '/../src/vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \lbs\auth\api\bootstrap\Eloquent as Eloquent;

// Configuration de Slim (réécriture des erreurs + settings du container Slim)
$errors = require_once __DIR__ . '/../src/api/errors/errors.php';
$settings = require_once __DIR__ . '/../src/api/settings/settings.php';

$slim_config = array_merge($errors, $settings);

$slim_container = new \Slim\Container($slim_config);

// Initialisation de l'application Slim
$app = new \Slim\App($slim_container);

// Initialisation d'Eloquent
Eloquent::start(($app->getContainer())->settings['dbconf']);

// Authentification
$app->post('/auth[/]', \lbs\auth\api\controller\LBSAuthController::class . ":authenticate")->setName('authentification');

// Vérification du token d'authentification
$app->get('/check[/]', \lbs\auth\api\controller\LBSAuthController::class . ":checkJWTToken")->setName('checkToken');

$app->run();