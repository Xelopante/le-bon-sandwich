<?php
/**
 * File:  index.php
 *
 */

require_once  __DIR__ . '/../src/vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//Configuration de Slim (réécriture des erreurs + settings du container Slim)
$errors = require_once __DIR__ . '/../src/api/errors/errors.php';
$settings = require_once __DIR__ . '/../src/api/settings/settings.php';

$slim_config = array_merge($errors, $settings);

$slim_container = new \Slim\Container($slim_config);

//Initialisation de l'application Slim
$app = new \Slim\App($slim_container);

$app->get('/test[/]', function(Request $req, Response $resp, $args) {
    //$resp->getBody()->write(json_encode(["message" => "coucou"]));
    //$resp = $resp->withHeader('Content-Type', 'application/json;charset=utf-8');

    return $this->view->render($resp, 'test.html.twig', ["message" => "coucou"]);
})->setName("getCommands");

$app->get('/sandwiches/{id}[/]', lbs\wrapper\api\controller\WrapperController::class . ":getSandwich")->setName('getSandwich');

$app->get('/categories[/]', lbs\wrapper\api\controller\WrapperController::class . ":getCategories")->setName('getCategories');

$app->get('/categories/{libelle}[/]', lbs\wrapper\api\controller\WrapperController::class . ":getCategorieSandwiches")->setName('getCategorieSandwiches');

$app->run();