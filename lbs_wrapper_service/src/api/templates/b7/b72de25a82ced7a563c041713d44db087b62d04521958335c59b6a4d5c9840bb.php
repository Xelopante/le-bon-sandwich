<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sandwich.html.twig */
class __TwigTemplate_e50f81ff3473021e1ee4e836dc8977dd856fe60ef34ab5650a14b2114843eddd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <title>Sandwich - ";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_0 = ($context["data"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null), "libelle", [], "any", false, false, false, 5), "html", null, true);
        echo "</title>
    <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
    <link href=\"https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,500;1,600;1,700&family=Secular+One&display=swap\" rel=\"stylesheet\">
</head>
<body>
    <div class=\"sandwich\">
        <h3>";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_1 = ($context["data"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[0] ?? null) : null), "libelle", [], "any", false, false, false, 12), "html", null, true);
        echo "</h3>
        <p>";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_2 = ($context["data"] ?? null)) && is_array($__internal_compile_2) || $__internal_compile_2 instanceof ArrayAccess ? ($__internal_compile_2[0] ?? null) : null), "description", [], "any", false, false, false, 13), "html", null, true);
        echo "</p>
        <p class=\"prix\">";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_3 = ($context["data"] ?? null)) && is_array($__internal_compile_3) || $__internal_compile_3 instanceof ArrayAccess ? ($__internal_compile_3[0] ?? null) : null), "prix", [], "any", false, false, false, 14), "html", null, true);
        echo "
    </div>

    <style>
        *, html, body{
            margin : 0;
            padding : 0;
            font-family : \"Josefin Sans\", sans-serif;
            overflow: hidden;
        }

        body{
            background : #072448;
            height: 100vh;
            width : 100vw;
            display : flex;
            flex-direction : column;
            align-items : center;
            justify-content : center;
        }

        h1{
            font-size : 40px;
            color : white;
            font-family : \"Secular One\", sans-serif;
            margin-bottom : 30px;
        }

        .sandwich{
            border : 15px solid rgba(255,255,255, .3);
            border-radius : 15px;
            box-shadow: 0 0 7px -1px white;
            padding : 20px;
            margin : 20px;
            display: flex;
            flex-direction : column;
            align-items : center;
            justify-content: center;
            text-align : center;
            width : 300px;
        }

        .sandwich:nth-child(1){
            background : #54D2D2;
        }

        .sandwich:nth-child(2){
            background : #FFCB00;
        }

        .sandwich:nth-child(3){
            background : #F8AA4B;
        }

        .sandwich:nth-child(4){
            background : #FF6150;
        }

        svg{
            width : 150px;
            height : auto;
        }

        h3{
            text-transform : capitalize;
            font-weight : 900;
            font-size : 20px;
            margin-bottom : 20px;
        }

        .prix{
            font-weight : 800;
            font-size : 20px;
            margin-top : 20px;
        }

    </style>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "sandwich.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 14,  57 => 13,  53 => 12,  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <title>Sandwich - {{data[0].libelle}}</title>
    <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
    <link href=\"https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,500;1,600;1,700&family=Secular+One&display=swap\" rel=\"stylesheet\">
</head>
<body>
    <div class=\"sandwich\">
        <h3>{{data[0].libelle}}</h3>
        <p>{{data[0].description}}</p>
        <p class=\"prix\">{{data[0].prix}}
    </div>

    <style>
        *, html, body{
            margin : 0;
            padding : 0;
            font-family : \"Josefin Sans\", sans-serif;
            overflow: hidden;
        }

        body{
            background : #072448;
            height: 100vh;
            width : 100vw;
            display : flex;
            flex-direction : column;
            align-items : center;
            justify-content : center;
        }

        h1{
            font-size : 40px;
            color : white;
            font-family : \"Secular One\", sans-serif;
            margin-bottom : 30px;
        }

        .sandwich{
            border : 15px solid rgba(255,255,255, .3);
            border-radius : 15px;
            box-shadow: 0 0 7px -1px white;
            padding : 20px;
            margin : 20px;
            display: flex;
            flex-direction : column;
            align-items : center;
            justify-content: center;
            text-align : center;
            width : 300px;
        }

        .sandwich:nth-child(1){
            background : #54D2D2;
        }

        .sandwich:nth-child(2){
            background : #FFCB00;
        }

        .sandwich:nth-child(3){
            background : #F8AA4B;
        }

        .sandwich:nth-child(4){
            background : #FF6150;
        }

        svg{
            width : 150px;
            height : auto;
        }

        h3{
            text-transform : capitalize;
            font-weight : 900;
            font-size : 20px;
            margin-bottom : 20px;
        }

        .prix{
            font-weight : 800;
            font-size : 20px;
            margin-top : 20px;
        }

    </style>
</body>
</html>", "sandwich.html.twig", "/var/www/src/api/templates/sandwich.html.twig");
    }
}
