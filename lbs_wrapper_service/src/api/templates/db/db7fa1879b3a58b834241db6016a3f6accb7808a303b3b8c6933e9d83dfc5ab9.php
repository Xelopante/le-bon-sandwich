<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* erreur404.html.twig */
class __TwigTemplate_6dc459fcf59e9884da8f714580ef0280ce38c9cca2b4b3006b8264b4a44b4ccc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <title>Erreur 404</title>
</head>
<body>
    <h1>Erreur 404</h1>
    <p>Aucun objet trouvé</p>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "erreur404.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <title>Erreur 404</title>
</head>
<body>
    <h1>Erreur 404</h1>
    <p>Aucun objet trouvé</p>
</body>
</html>", "erreur404.html.twig", "/var/www/src/api/templates/erreur404.html.twig");
    }
}
