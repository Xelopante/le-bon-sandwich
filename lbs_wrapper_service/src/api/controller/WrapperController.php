<?php

namespace lbs\wrapper\api\controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \lbs\wrapper\api\utils\JsonWriter;
use Slim\Container;
use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

/**
 * Class WrapperController
 * @package lbs\wrapper\api\controller
 */
class WrapperController {

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Récupère un sandwich par son ID
     */
    public function getSandwich(Request $req, Response $resp, $args) {

        // Redirection de la requête vers directus
        $client = new Client([
            'base_uri' => $this->container->get('settings')['catalogue_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->get('/items/sandwiches?filter[id]='.$args["id"].'&fields[]=id,libelle,description,prix');
        }
        catch(ServerException | ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        // Mise des données dans le template
        $response_data = json_decode($response->getBody(), true);

        if(empty($response_data["data"])) {
            return $this->container->get('view')->render($resp, 'erreur404.html.twig');
        }

        return $this->container->get('view')->render($resp, 'sandwich.html.twig', $response_data);
    }

    /**
     * Récupère toutes les catégories de sandwich
     */
    public function getCategories(Request $req, Response $resp, $args) {

        // Redirection de la requête vers directus
        $client = new Client([
            'base_uri' => $this->container->get('settings')['catalogue_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->get('/items/categories');
        }
        catch(ServerException | ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        $response_data = json_decode($response->getBody(), true);

        if(empty($response_data["data"])) {
            return $this->container->get('view')->render($resp, 'erreur404.html.twig');
        }

        return $this->container->get('view')->render($resp, 'categories.html.twig', $response_data);
    }

    /**
     * Récupère les sandwiches d'une catégorie choisie par son libelle
     */
    public function getCategorieSandwiches(Request $req, Response $resp, $args) {

        // Redirection de la requête vers directus
        $client = new Client([
            'base_uri' => $this->container->get('settings')['catalogue_service'],
            'timeout' => 5.0
        ]);

        try {
            $response = $client->get('/items/categories?filter[libelle]='.$args['libelle'].'&fields[]=libelle,sandwiches.*');
        }
        catch(ServerException | ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        $response_data = json_decode($response->getBody(), true);

        if(empty($response_data["data"])) {
            return $this->container->get('view')->render($resp, 'erreur404.html.twig');
        }

        return $this->container->get('view')->render($resp, 'sandwiches.html.twig', $response_data);
    }
}

?>