<?php

return [
    "settings" => [
        "displayErrorDetails" => true,
        "catalogue_service" => "http://api.catalogue.local:8055",
        "tmpl_dir" => __DIR__ . "/../templates/"
    ],
    //Déclaration du service twig-view pour Slim
    "view" =>   function($container) {
                    return new \Slim\Views\Twig(
                        $container['settings']['tmpl_dir'],[
                            'debug' => true,'cache' =>$container['settings']['tmpl_dir']
                    ]);
        }
];

?>