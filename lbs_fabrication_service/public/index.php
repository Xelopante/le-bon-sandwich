<?php
/**
 * File:  index.php
 *
 */

require_once  __DIR__ . '/../src/vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \lbs\fab\app\bootstrap\Eloquent as Eloquent;

//Configuration de Slim (réécriture des erreurs + settings du container Slim)
$errors = require_once __DIR__ . '/../src/app/errors/errors.php';
$settings = require_once __DIR__ . '/../src/app/settings/settings.php';

$slim_config = array_merge($errors, $settings);

$slim_container = new \Slim\Container($slim_config);

//Initialisation de l'application Slim
$app = new \Slim\App($slim_container);

Eloquent::start(($app->getContainer())->settings['dbconf']);

$app->get("/commands[/]", \lbs\fab\app\controller\FabricationController::class . ":getCommands")->setName("getCommands");

$app->get("/commands/{id}[/]", \lbs\fab\app\controller\FabricationController::class . ":getCommand")->setName("getCommand");

$app->put("/commands/{id}[/]", \lbs\fab\app\controller\FabricationController::class . ":validCommand")->setName("validCommand");

$app->run();