<?php

namespace lbs\fab\app\controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \lbs\fab\app\utils\JsonWriter;
use \lbs\fab\app\models as Models;
use Slim\Container;

/**
 * Class FabricationController
 * @package lbs\fab\app\controller
 */
class FabricationController {

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Récupère toutes les commandes passées et en retourne un nombre et une taille donnés.
     */
    public function getCommands(Request $req, Response $resp, array $args) : Response {

        try {
            $commands = null;
            $status = $req->getQueryParam('s', null);

            // On vérifie si un paramèter de status existe pour rédcupérer les données en conséquence
            if($status != null) {
                $commands = Models\Commande::getAllCommandesByStatus($status);
            }
            else {
                $commands = Models\Commande::getAllCommandes();
            }

            /// PAGE & SIZE ///
            /// On gère un système de page grâce à une taille. Si la page 1 retourne 10 éléments, la page suivante retourne les 10 éléments d'après à partir du 11ème, etc.
            /// Pour faire ça on calcule un offset en fonction de la page (voir $offset) et de la taille de la page fournie.

            $size = $req->getQueryParam('size', null);
            // Si un paramètre de taille existe et est valide, on l'applique. Sinon la valeur par défaut est 10 commandes par page.
            $size = (($size != null) && ($size > 0)) ? intval($size) : 10;
            // Si la taille demandée est + grande que le nombre de commandes, on met la taille au nombre total de commandes.
            if($size > count($commands)) {
                $size = count($commands);
            }

            $page = $req->getQueryParam('page', null);
            // Si un paramètre de page existe et est valide, on l'applique. Sinon la valeur par défaut est page 1.
            $page = (($page != null) && ($page > 0)) ? intval($page) : 1;
            // Si la page demandée est + grande que le nombre de page total, on retourne la dernière page existant.
            if($page > (count($commands) / $size)) {
                $page = ceil(count($commands) / $size);
            }

            $firstPage = 1;
            $lastPage = ceil(count($commands) / $size);
            // Si on est à la page max, il n'y a pas de prochaine page
            $nextPage = ($page < $lastPage ? $page + 1 : null);
            // Si on est à la première page, il n'y a pas de page précédente
            $previousPage = ($page > 1 ? $page - 1 : null);

            $links = [
                "first" => [
                    "href" => $this->container['router']->urlFor('getCommands') . "?page=".$firstPage."&size=".$size
                ],
                "last" => [
                    "href" => $this->container['router']->urlFor('getCommands') . "?page=".$lastPage."&size=".$size
                ]
            ];

            if($nextPage != null) {
                $links["next"] = [
                    "href" => $this->container['router']->urlFor('getCommands') . "?page=".$nextPage."&size=".$size
                ];
            }

            if($previousPage != null) {
                $links["prev"] = [
                    "href" => $this->container['router']->urlFor('getCommands') . "?page=".$previousPage."&size=".$size
                ];
            }

            $offset = ($size * ($page - 1));

            $data = array();
            $data["type"] = "collection";
            $data["count"] = count($commands);
            $data["size"] = $size;
            $data["links"] = $links;
            $data["commands"] = [];

            if(count($commands) != 0) {
                // Récupération des commandes en fonction de la page et de la taille
                $commands_sliced = array_slice($commands->toArray(), $offset, $size);

                foreach($commands_sliced as $command) {
                    $command["links"] = [
                        "self" => [
                            "href" => $this->container['router']->urlFor('getCommand', ['id' => $command['id']])
                        ]
                    ];

                    $data["commands"][] = [
                        "command" => $command
                    ];
                }
            }
        }
        catch(ModelNotFoundException $e) {
            $data = [
                "type" => "error",
                "error" => 404,
                "message" => "resource not available : " . $this->container['router']->urlFor('getCommandes') . "?page=".$page."&size=".$size
            ];

            return JsonWriter::jsonOutput($resp, 404, $data);
        }
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return JsonWriter::jsonOutput($resp, 200, $data);
    }

    /**
     * On récupère une commande par son ID
     */
    public function getCommand(Request $req, Response $resp, array $args) : Response {

        $id = $args['id'];

        try {
            $data["type"] = "resource";
            $data["command"] = Models\Commande::getCommandeByIdWithItems($id);
        }
        // On inclue une gestion des erreurs si l'objet n'est pas trouvé, erreur 404
        catch(ModelNotFoundException $e) {
            $data = [
                "type" => "error",
                "error" => 404,
                "message" => "resource not available : " . $this->container['router']->urlFor('getCommand', [ 'id' => $id ])
            ];

            return JsonWriter::jsonOutput($resp, 404, $data);
        }
        // Pour tout autre erreur de traitement, on retourne une erreur 500
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return JsonWriter::jsonOutput($resp, 200, $data);
    }

    /**
     * Change le statut d'une commande pour indiquer un paiement effectué
     * /!\ Fonction liée à orderCommand du suivi de commande, on ne peut encore une fois vérifier l'exactitude des infos de paiement et du solde de l'utilisateur /!\
     */
    public function validCommand(Request $req, Response $resp, $args) : Response {

        $id = $args['id'];

        try {
            $commande = Models\Commande::getCommandeById($id);

            // 3 correspond à la prise en charge, 4 à une commande prête
            if($commande->status > 2) {
                
                $data = [
                    "type" => "error",
                    "error" => 401,
                    "message" => "Command have already been processed"
                ];
    
                return JsonWriter::jsonOutput($resp, 401, $data);
            }

            // On vérifie que le paiement a été effectué dans la base
            $paiement = Models\Paiement::getPaiementById($commande->id);

            if($paiement == null) {
                throw new ModelNotFoundException();
            }

            // On passe direct à 4 dans la simulation de la prise de commande de sandwiches
            $commande->status = 4;
            $commande->save();
        }
        // On inclue une gestion des erreurs si l'objet n'est pas trouvé, erreur 404
        catch(ModelNotFoundException $e) {
            $data = [
                "type" => "error",
                "error" => 404,
                "message" => "Command not found or not paid"
            ];

            return JsonWriter::jsonOutput($resp, 404, $data);
        }
        // Pour tout autre erreur de traitement, on retourne une erreur 500
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Wrong data inserted in DB"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return JsonWriter::jsonOutput($resp, 204);
    }
}

?>