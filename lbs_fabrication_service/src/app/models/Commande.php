<?php

namespace lbs\fab\app\models;
use \Illuminate\Database\Eloquent\Model;

/**
 * Class Commande
 * @package lbs\fab\app\models
 */
class Commande extends Model {
    
    protected $table = 'commande';
    protected $primaryKey = 'id';

    public $incrementing = false;
    public $keyType='string';

    /* === Fonctions de relation entre tables === */

    /**
     * Retourne les items associés à la commande courante
     */
    public function items() {
        return $this->hasMany('\lbs\fab\app\models\Item', 'command_id');
    }

    /* ========================================== */

    /**
     * Récupération de la commande par son Id
     */
    public static function getCommandeById($id) {
        return self::select('id', 'nom', 'mail', 'montant', 'status')->where('id', '=', $id)->firstOrFail();
    }

    /**
     * Récupération de la commande par son Id avec les items associé
     */
    public static function getCommandeByIdwithItems($id) {
        return self::select('id', 'nom', 'mail', 'montant', 'status')->where('id', '=', $id)->with(['items'])->firstOrFail();
    }

    /**
     * Récupération de toutes les commandes rangées par ordre de date de livraisonpuis date de création
     */
    public static function getAllCommandes() {
        return self::select('id', 'nom', 'created_at', 'livraison', 'status')->orderBy('livraison', 'asc')->orderBy('created_at', 'asc')->get();
    }

    /**
     * Récupération de toutes les commandes triées par status
     */
    public static function getAllCommandesByStatus($status) {
        return self::select('id', 'nom', 'created_at', 'livraison', 'status')->where('status', '=', $status)->orderBy('livraison', 'asc')->orderBy('created_at', 'asc')->get();
    }

    /**
     * Update/Création d'une commande
     */
    public function saveCommande($id = null, $nom, $mail, $livraison, $montant, $token) {
        if($id != null) {
            $this->id = $id;
        }
        
        $this->nom = $nom;
        $this->mail = $mail;
        $this->livraison = $livraison;
        $this->montant = $montant;
        $this->token = $token;

        return $this->save();
    }
}

?>