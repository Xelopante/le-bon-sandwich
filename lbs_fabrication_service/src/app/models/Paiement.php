<?php

namespace lbs\fab\app\models;
use \Illuminate\Database\Eloquent\Model;

/**
 * Class Paiement
 * @package lbs\fab\app\models
 */
class Paiement extends Model {
    
    protected $table = 'paiement';
    protected $primaryKey = 'commande_id';

    public $incrementing = false;
    public $keyType='string';

    /**
     * Retourne un paiement par son id de commande
     */
    public static function getPaiementById($id) {
        return self::select('commande_id', 'reference', 'montant_paiement')->where('commande_id', '=', $id)->firstOrFail();
    }
}

?>