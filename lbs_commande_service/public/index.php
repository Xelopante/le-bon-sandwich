<?php
/**
 * File:  index.php
 *
 */

require_once  __DIR__ . '/../src/vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \lbs\command\app\bootstrap\Eloquent as Eloquent;

use \DavidePastore\Slim\Validation\Validation as Validation;
use \lbs\command\app\middlewares\Validator as Validator;

use \lbs\command\app\middlewares\TokenControl;

// Configuration de Slim (réécriture des erreurs + settings du container Slim)
$errors = require_once __DIR__ . '/../src/app/errors/errors.php';
$settings = require_once __DIR__ . '/../src/app/settings/settings.php';

$slim_config = array_merge($errors, $settings);

$slim_container = new \Slim\Container($slim_config);

// Initialisation de l'application Slim
$app = new \Slim\App($slim_container);

// Initialisation d'Eloquent
Eloquent::start(($app->getContainer())->settings['dbconf']);

$app->get('/commands/{id}[/]', \lbs\command\app\controller\CommandeController::class . ':getCommand')->setName('getCommand')->add(TokenControl::class.':checkToken');

$app->get('/commands/{id}/items[/]', \lbs\command\app\controller\CommandeController::class . ':getCommandItems')->setName('getCommandItems')->add(TokenControl::class.':checkToken');

$app->post('/commands[/]', \lbs\command\app\controller\CommandeController::class . ':createCommand')->setName('newCommand')
->add(new Validation(Validator::getPostCommandeValidator()));

$app->put('/commands/{id}[/]', \lbs\command\app\controller\CommandeController::class . ':orderCommand')->add(TokenControl::class.':checkToken')->add(new Validation(Validator::getOrderCommandeValidator()))->setName('orderCommand');

$app->run();