<?php

namespace lbs\command\app\middlewares;

use Respect\Validation\Validator as v;

/**
 * Class Validator
 * @package lbs\command\app\middlewares
 */
class Validator {

    // Tableau de validation pour la création d'une commande
    public static function getPostCommandeValidator() : array {

        return [
            'mail' => v::email(),
            'nom' => v::stringType()->alpha(),
            'livraison' => [
                'date' => v::date('Y-m-d')->min('now'),
                'heure' => v::date('H:i:s')
            ],
            'items' => [[
                'uri' => v::stringType()->alnum('/'),
                'quantite' => v::intType(),
            ]]
        ];
    }

    // Tableau de validation pour le paiement d'une commande
    public static function getOrderCommandeValidator() : array {

        return [
            'numero' => v::creditCard(),
            'date-expiration' => v::date('m-y')
        ];
    }
}

?>