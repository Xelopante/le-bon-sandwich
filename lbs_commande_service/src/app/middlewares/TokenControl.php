<?php

namespace lbs\command\app\middlewares;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \lbs\command\app\utils\JsonWriter;
use \lbs\command\app\models as Models;

/**
 * Class TokenControl
 * @package lbs\command\app\middlewares
 */
class TokenControl {

    /**
     * Middleware qui vérifie que le token passé est le même que celui de la commande recherchée
     */
    public static function checkToken(Request $req, Response $resp, callable $next) : Response {

        // On vérifie l'existence du token soit dans les headers, soit dans les paramètres de l'URL
        $token = $req->getQueryParam('token', null);

        if(($token === null) && (!empty($req->getHeader('X-lbs-token')))) {
            $token = $req->getHeader('X-lbs-token')[0];
        }

        $id = $req->getAttribute('route')->getArgument('id');

        // On vérifie d'abord si la commande existe
        try {
            Models\Commande::where('id', '=', $id)
                            ->firstOrFail();
        }
        catch(ModelNotFoundException $e) {
            $data = [
                "type" => "error",
                "error" => 404,
                "message" => "Command does not exist"
            ];

            return JsonWriter::jsonOutput($resp, 404, $data);
        }

        // Puis si son token est le même que celui transmis
        try {
            Models\Commande::where('id', '=', $id)
                            ->where('token', '=', $token)
                            ->firstOrFail();
        }
        catch(ModelNotFoundException $e) {
            $data = [
                "type" => "error",
                "error" => 404,
                "message" => "Invalid token"
            ];

            return JsonWriter::jsonOutput($resp, 404, $data);
        }

        return $next($req, $resp);
    }
}

?>
