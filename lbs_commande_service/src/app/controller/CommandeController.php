<?php

namespace lbs\command\app\controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \lbs\command\app\models as Models;
use lbs\command\app\utils\JsonWriter;
use Ramsey\Uuid\Uuid;
use Slim\Container;
use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\RequestOptions;

/**
 * Class CommandeController
 * @package lbs\command\app\controller
 */
class CommandeController {

    protected $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }

    /**
     * Récupère une commande par son ID
    */
    public function getCommand(Request $req, Response $resp, array $args) : Response {

        $id = $args['id'];

        try {
            $data["type"] = "resource";
            // On vérifie si le paramètre embed existe et est valide pour adapter la requête à la BDD
            if(isset($_GET["embed"]) && $_GET["embed"] != null) {
                $data["commande"] = Models\Commande::getCommandeByIdWithEmbed($id, $_GET["embed"]);
            }
            else {
                $data["commande"] = Models\Commande::getCommandeById($id);
            }

            //On inclue les liens dans le JSON
            $data["links"] = [
                "self" => $this->container['router']->urlFor('getCommand', [ 'id' => $id ]),
                "items" =>  $this->container['router']->urlFor('getCommandItems', [ 'id' => $id ])
            ];
        }
        // Si l'objet n'est pas trouvé
        catch(ModelNotFoundException $e) {
            $data = [
                "type" => "error",
                "error" => 404,
                "message" => "resource not available : " . $this->container['router']->urlFor('getCommandeTD2', [ 'id' => $id ])
            ];

            return JsonWriter::jsonOutput($resp, 404, $data);
        }
        // Si la relation *embed* n'existe pas
        catch(RelationNotFoundException $e) {
            $data = [
                "type" => "error",
                "error" => 404,
                "message" => "Requested embed does not exist"
            ];

            return JsonWriter::jsonOutput($resp, 404, $data);
        }

        return JsonWriter::jsonOutput($resp, 200, $data);
    }

    /**
     * Récupère les items d'une commande récupérée par son ID
    */
    public function getCommandItems(Request $req, Response $resp, array $args) : Response {

        $id = $args['id'];

        try {
            $data["type"] = "collection";
            $data["count"] = 0;
            $data["items"] = Models\Commande::getCommandeById($id)->items()->select('id', 'libelle', 'tarif', 'quantite')->get();
            $data["count"] = count($data["items"]);
        }
        // On inclue une gestion des erreurs si l'objet n'est pas trouvé, erreur 404
        catch(ModelNotFoundException $e) {
            $data = [
                "type" => "error",
                "error" => 404,
                "message" => "resource not available : " . $this->container['router']->urlFor('getCommandeTD4', [ 'id' => $id ])
            ];

            return JsonWriter::jsonOutput($resp, 404, $data);
        }
        // Pour toute autre erreur de traitement, on retourne une erreur 500
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Internal PHP error"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return JsonWriter::jsonOutput($resp, 200, $data);
    }

    /**
     * Crée une nouvelle commande
     */
    public function createCommand(Request $req, Response $resp, array $args) : Response {

        // On check si le validateur a retourné des erreurs lors de l'envoie des données
        if($req->getAttribute('has_errors')) {
            
            $errors = $req->getAttribute('errors');

            $data = [
                "type" => "error",
                "code" => 400,
                "message" => $errors
            ];

            return JsonWriter::jsonOutput($resp, 400, $data);
        }

        try {
            $commande = new Models\Commande();

            $request_body = $req->getParsedBody();

            $id = Uuid::uuid4()->toString();

            // On récupère les champs de la commande du body de la requête
            $mail = $request_body["mail"];
            $nom = $request_body["nom"];
            $date = $request_body["livraison"]["date"];
            $heure = $request_body["livraison"]["heure"];
            $livraison = $date . " " . $heure;
            $montant = 0;

            // On récupère et insert les champs des items de la commande du body de la requête
            foreach($request_body["items"] as $item_link) {

                // On récupère les infos du sandwich via son uri dans le catalogue
                $client = new Client([
                    'base_uri' => $this->container->get('settings')['catalogue_service'],
                    'timeout' => 5.0
                ]);
        
                $response = $client->get('items/' . $item_link["uri"]);

                $item_data = json_decode($response->getBody()->getContents(), true);

                $tarif = $item_data["data"]["prix"];
                $libelle = $item_data["data"]["libelle"];
                $uri = $item_link["uri"];
                $quantite = $item_link["quantite"];
                $command_id = $id;

                // On incrémente le montant total de la commande
                $montant += ($tarif * $quantite);

                $item = new Models\Item();

                $item->saveItem($uri, $libelle, $tarif, $quantite, $command_id);
            }

            // On crée le token d'identification de la commande
            $token = random_bytes(32);
            $token = bin2hex($token);

            $commande->saveCommande($id, $mail, $nom, $livraison, $montant, $token);

            // On retourne la commande créée
            $data = [
                "commande" => [
                    "nom" => $commande->nom,
                    "mail" => $commande->mail,
                    "date_livraison" => $commande->livraison,
                    "id" => $commande->id,
                    "token" => $commande->token,
                    "montant" => $commande->montant
                ]
            ];
        }
        // Erreur de serveur / client guzzle
        catch(ServerException | ClientException $e) {
            $data = json_decode($e->getResponse()->getBody(), true);

            return JsonWriter::jsonOutput($resp, $data["error"], $data);
        }
        // Pour toute autre erreur de traitement, on retourne une erreur 500
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Wrong data inserted in DB"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        $resp = $resp->withHeader('Location', $this->container['router']->pathFor('getCommand', [ 'id' => $commande->id ]));

        return JsonWriter::jsonOutput($resp, 201, $data);
    }

    /**
     * Change le statut d'une commande pour indiquer un paiement effectué
     * /!\ Etant donné que nous ne sommes pas en mesure de vérifier les informations bancaires via une API, cette route est uniquement une simulation de paiement /!\
     */
    public function orderCommand(Request $req, Response $resp, $args) : Response {

        // On check si le validateur a retourné des erreurs lors de l'envoie des données
        if($req->getAttribute('has_errors')) {
            
            $errors = $req->getAttribute('errors');

            $data = [
                "type" => "error",
                "code" => 400,
                "message" => $errors
            ];

            return JsonWriter::jsonOutput($resp, 400, $data);
        }

        $id = $args['id'];

        try {
            $commande = Models\Commande::getCommandeById($id);

            // 2 correspond à un paiement effectué et valide
            if($commande->status > 1) {
                
                $data = [
                    "type" => "error",
                    "error" => 401,
                    "message" => "Command have already been paid"
                ];
    
                return JsonWriter::jsonOutput($resp, 401, $data);
            }

            $request_body = $req->getParsedBody();

            // On récupère les champs pour le paiement (numero CB + date expiration)
            $numeroCb = $request_body["numero"];
            $dateExpiration = $request_body["date-expiration"];

            $paiement = new Models\Paiement();

            $paiement->commande_id = $commande->id;
            $paiement->reference = Uuid::uuid4()->toString();
            $paiement->montant_paiement = $commande->montant;
            $paiement->mode_paiement = 1; //Supposons que 1 correspond à la CB

            $paiement->save();

            $commande->status = 2;
            $commande->save();
        }
        // Pour toute autre erreur de traitement, on retourne une erreur 500
        catch(\Exception $e) {
            $data = [
                "type" => "error",
                "error" => 500,
                "message" => "Wrong data inserted in DB"
            ];

            return JsonWriter::jsonOutput($resp, 500, $data);
        }

        return JsonWriter::jsonOutput($resp, 204);
    }
}

?>