<?php

namespace lbs\command\app\bootstrap;
use Illuminate\Database\Capsule\Manager as EloquentManager;

/**
 * Class Eloquent
 * @package lbs\command\app\bootstrap
 */
class Eloquent {

    /**
     * Récupère les informations de connexion à la BDD pour démarrer Eloquent
     * 
     * @param string $conf_file_path Chemin du fichier des informations de connexion à la BDD
     */
    public static function start(string $conf_file_path) {

        $config = parse_ini_file($conf_file_path);

        $eloquent = new EloquentManager();
        $eloquent->addConnection($config);
        $eloquent->setAsGlobal();
        $eloquent->bootEloquent();
    }
}

?>