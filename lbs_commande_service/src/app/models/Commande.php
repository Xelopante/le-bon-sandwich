<?php

namespace lbs\command\app\models;
use \Illuminate\Database\Eloquent\Model;

/**
 * Class Commande
 * @package lbs\command\app\models
 */
class Commande extends Model {
    
    protected $table = 'commande';
    protected $primaryKey = 'id';

    public $incrementing = false;
    public $keyType='string';

    /* === Fonctions de relation entre tables === */

    /**
     * Retourne les items associés à la commande courante
     */
    public function items() {
        return $this->hasMany('\lbs\command\app\models\Item', 'command_id');
    }

    /* ========================================== */

    /**
     * Récupération de la commande par son Id
     */
    public static function getCommandeById($id) {
        return self::select('id', 'mail', 'montant', 'status')->where('id', '=', $id)->firstOrFail();
    }

    /**
     * Récupération de la commande par son Id + les colonnes de la table associée demandée
     */
    public static function getCommandeByIdWithEmbed($id, $embed) {
        return self::select('id', 'mail', 'montant')->where('id', '=', $id)->with($embed)->get();
    }

    /**
     * Récupération de toutes les commandes
     */
    public static function getAllCommandes() {
        return self::select('id', 'mail', 'montant')->get();
    }

    /**
     * Update/Création d'une commande
     */
    public function saveCommande($id = null, $nom, $mail, $livraison, $montant, $token) {
        if($id != null) {
            $this->id = $id;
        }
        
        $this->nom = $nom;
        $this->mail = $mail;
        $this->livraison = $livraison;
        $this->montant = $montant;
        $this->token = $token;

        return $this->save();
    }
}

?>