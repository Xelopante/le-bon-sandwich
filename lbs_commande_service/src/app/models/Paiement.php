<?php

namespace lbs\command\app\models;
use \Illuminate\Database\Eloquent\Model;

/**
 * Class Paiement
 * @package lbs\command\app\models
 */
class Paiement extends Model {
    
    protected $table = 'paiement';
    protected $primaryKey = 'commande_id';

    public $incrementing = false;
    public $keyType='string';
}

?>