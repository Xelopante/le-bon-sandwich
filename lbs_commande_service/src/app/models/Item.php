<?php

namespace lbs\command\app\models;
use \Illuminate\Database\Eloquent\Model;

/**
 * Class Item
 * @package lbs\command\app\models
 */
class Item extends Model {
    
    protected $table = 'item';
    protected $primaryKey = 'id';

    public $timestamps = false;

    /* === Fonctions de relation entre tables === */

    /**
     * Retourne les items associés à la commande courante
     */
    public function commande() {
        return $this->belongsTo('\lbs\command\app\models\Commande', 'command_id');
    }

    /* ========================================== */

    /**
     * Sauvegarde un item
     */
    public function saveItem($uri, $libelle, $tarif, $quantite, $command_id, $id = null) {
        if($id != null) {
            $this->id = $id;
        }

        $this->uri = $uri;
        $this->libelle = $libelle;
        $this->tarif = $tarif;
        $this->quantite = $quantite;
        $this->command_id = $command_id;

        return $this->save();
    }
}

?>