== TD Applications et Services Web - Le bon sandwich ==

```diff
@@ Mise en place des services @@
```

```
1. git pull

2. docker-compose -f docker-compose.yml up

3. docker-compose start
```

4. Récupérer les id des container via la commande `docker ps` puis pour chaque id :

```
5. docker exec -it *id* /bin/bash

6. composer install
```

```diff
@@ Listes des hosts à ajouter @@
```
```
# Dev. Applis & Services web
127.0.0.1 api.commande.local
127.0.0.1 api.fabrication.local
127.0.0.1 api.auth.local
127.0.0.1 api.backoffice.local
127.0.0.1 api.catalogue.local
127.0.0.1 api.wrapper.local
127.0.0.1 api.frontoffice.local
```